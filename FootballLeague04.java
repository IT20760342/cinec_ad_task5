import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class FootballLeague04 {
	public static void main(String [] args) {
		List <FootballClub> table = new ArrayList<>();
		
		try (BufferedReader buffered = new BufferedReader(new FileReader("input.csv"))) {
		    String line;
		    while ((line = buffered.readLine()) != null) {
		        String[] value = line.split(",");
		        table.add(new FootballClub(Integer.parseInt(value[0]),value[1],Integer.parseInt(value[2]),Integer.parseInt(value[3]),Integer.parseInt(value[4]),Integer.parseInt(value[5]),Integer.parseInt(value[6]),Integer.parseInt(value[7]), Double.parseDouble(value[8])));
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
//		Filter and forEach
		System.out.println("\nTeams won more than 8 matches");
		System.out.println("------------------------------\n");
		System.out.println("   Team Name                   won      drawn     lost     points\n");
	    table.stream().filter(FootballClub -> FootballClub.getWon() > 8)
	        .forEach(System.out::println);

	    System.out.println();
	    System.out.println("Teams lost more than 5 matches");
	    System.out.println("------------------------------\n");
	    System.out.println("   Team Name                   won      drawn     lost     points\n");
	    table.stream().filter(FootballClub -> FootballClub.getLost() > 5)
	        .forEach(System.out::println);
	    
	    
//	    Write same results to file
	    try {
		      FileWriter writer = new FileWriter("Output4.txt");
		      writer.write("Teams won more than 8 matches\n");
		      writer.write("------------------------------\n\n");
		      writer.write("   Team Name                   won      drawn     lost     points\n");
		      writer.write("   ---------                   ---      ------    ----     ------\n");
		      table.stream().filter(FootballClub -> FootballClub.getWon() > 8)
		        .forEach(str -> {
		        	try {
		        		writer.write(str.toString() + "\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
		        });
		      
		      writer.write("\nTeams lost more than 5 matches\n");
		      writer.write("------------------------------\n\n");
		      writer.write("   Team Name                   won      drawn     lost     points\n");
		      writer.write("   ---------                   ---      ------    ----     ------\n");
		      table.stream().filter(FootballClub -> FootballClub.getLost() > 5)
		        .forEach(str -> {
		        	try {
		        		writer.write(str.toString() + "\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
		        });
		      writer.close();
		      System.out.println("\nSuccessfully wrote to the file Output4.txt.");
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
	}
}
